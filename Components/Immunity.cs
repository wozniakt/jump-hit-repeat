﻿using UnityEngine;
using System.Collections;

public class Immunity : MonoBehaviour
{ Flickering flickering;
	public float timeOfImmunity;
	int defaultLayer;
	// Use this for initialization
	void Start(){
		flickering = GetComponent<Flickering> ();
		defaultLayer = gameObject.layer;
	}
	public void BeImmune ()
	{
		
		StartCoroutine (ImmuneAfterHit());
	}
	
	// Update is called once per frame
	IEnumerator ImmuneAfterHit ()
	{
		if (this.CompareTag("Monster")) {
			gameObject.layer = 17;//immmunity layer
			flickering.startFlickering();
			yield return new WaitForSeconds(timeOfImmunity);
			gameObject.layer = defaultLayer;
			
		} else {
			gameObject.layer = 14;//immmunity layer
			yield return new WaitForSeconds(timeOfImmunity);
			gameObject.layer = defaultLayer;
		}
	
	}
}

