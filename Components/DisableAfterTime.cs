﻿using UnityEngine;
using System.Collections;
using System;

public class DisableAfterTime : MonoBehaviour
{
	public float timeToDisable;
	// Use this for initialization
	void OnEnable ()
	{
		//timeToDisable = 0;
//		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		StartCoroutine (disable ());

	}


	// Update is called once per frame
	IEnumerator disable ()
	{

		yield return new WaitForSeconds (timeToDisable);
//		GameObject obj = (GameObject)Instantiate(Resources.Load("Prefabs/" +"Prefabs_Heroes/"+"text///////Debug"));
//		obj.transform.position = new Vector2( transform.position.x,UnityEngine.Random.Range(-2,2));
//		obj.GetComponent<TextMesh> ().text = "|"+ Math.Round(this.GetComponent<StopAfterTime> ().timeToDisable,2).ToString();
		this.gameObject.SetActive (false);
		//	Destroy (this.gameObject);
	}
}

