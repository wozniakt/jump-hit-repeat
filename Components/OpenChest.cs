﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class OpenChest : MonoBehaviour
{
	Chest oChest;
	// Use this for initialization
	void OnEnable ()
	{
		oChest = GetComponent<ChestInit> ().oChest;
		Sprite newSprite =(Resources.Load<Sprite> ("Sprites/Other_sprites/ChestClosed"+oChest.Color));
		GetComponent<Image> ().sprite = newSprite;

	}
	
	// Update is called once per frame
	public void Open(){
		Sprite newSprite =(Resources.Load<Sprite> ("Sprites/Other_sprites/ChestOpened"+oChest.Color));
		GetComponent<Image> ().sprite = newSprite;
		//StoryManager.instance.storyCounter=0;
		for (int i = 0; i < oChest.Goblins; i++) {
			GameObject hero = PoolManager.instance.GetPooledObject_Hero (1000);
			hero.transform.position =new Vector3( -1+2*i,-2,2.5f);
			hero.gameObject.SetActive (true);
		}
		if (oChest.Gold>0) {
			ManagerOfGlobalEvents.instance.TriggerShowText (this.oChest.Gold.ToString(),transform.localPosition);
			ManagerOfGlobalEvents.instance.TriggerOnGetPoints (oChest.Gold);
			GameObject coin=  PoolManager.instance.GetPooledObject_Effect (EffectType.Coin);
			coin.transform.position = new Vector3( 0, -3,2.5f);
			coin.SetActive (true);
		}
		//StoryManager.instance.ShowChests (false);
		StoryManager.instance.reInitStory();
	}
}

