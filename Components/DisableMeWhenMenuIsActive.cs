﻿using UnityEngine;
using System.Collections;

public class DisableMeWhenMenuIsActive : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		ManagerOfGlobalEvents.instance.OnChangeGameState += this.DisableMe;
	}
	
	// Update is called once per frame
	void Disable ()
	{
		ManagerOfGlobalEvents.instance.OnChangeGameState -= this.DisableMe;
	}
	void DisableMe(GameState oGameState){
		if (oGameState==GameState.Menu && this!=null) {
			this.gameObject.SetActive (false);
		}
	}
}

