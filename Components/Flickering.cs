﻿using UnityEngine;
using System.Collections;

public class Flickering : MonoBehaviour
{
	[HideInInspector]
	public Renderer renderer;
	public float DelayTime, IntervalTime, FlickCount;
	// Use this for initialization
	void OnEnable ()
	{
		//renderer =new Renderer();
		renderer = GetComponent<Renderer> ();
		//StartCoroutine (flickering (DelayTime, IntervalTime, FlickCount));
	}

	public void startFlickering(){
		StartCoroutine (flickering (DelayTime, IntervalTime, FlickCount));
	}

	// Update is called once per frame
	IEnumerator  flickering (float DelayTime, float IntervalTime,float flickCount)
	{
		yield return new WaitForSeconds(DelayTime);
		for (int i = 0; i < flickCount; i++) {
			
		
		for (float f = 1f; f >= 0; f -= 0.1f) {

			Color c = renderer.material.color;
			c.a = f;
			renderer.material.color = c;
			yield return null;
		}
		yield return new WaitForSeconds(IntervalTime);
		for (float f = 0f; f <= 1; f += 0.1f) {

			Color c = renderer.material.color;
			c.a = f;
			renderer.material.color = c;
			yield return null;
		}
		/////////Debug.Log ("Fade in");
		yield return new WaitForSeconds(IntervalTime);
		//StartCoroutine (flickering (0, IntervalTime,));
		}
	}


	void OnDisable(){
		Color c = renderer.material.color;
		c.a = 1;
		renderer.material.color = c;
		StopAllCoroutines ();
	}
}

