﻿using UnityEngine;
using System.Collections;

public class ExplodeOnCreature : MonoBehaviour
{
	public EffectType effectType;
	GameObject explodeGO;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.CompareTag("Hero") ) {
			explodeGO=PoolManager.instance.GetPooledObject_Effect (effectType);
			explodeGO.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y ,2);
			explodeGO.SetActive (true);
			this.gameObject.SetActive (false);
		}
	}
}

