﻿using UnityEngine;
using System.Collections;

public class TriggerAbility : MonoBehaviour
{


	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col)
	{
//	///////Debug.Log ("Colision");
		if (col.CompareTag("Monster")){
			SoundsManager.Instance.PlaySound (SoundType.PowerUp);
			//string abilityName = this.name;
//			///////Debug.Log (this.name+"Monster"+DataManager.instance.oPlayerData.CurrentMonster.Id);
			GameObject ability = PoolManager.instance.GetPooledObject_PowerUp ( this.name+"Monster"+DataManager.instance.oPlayerData.CurrentMonster.Id);
	
			if (ability.CompareTag("PowerUp0")) {
				ability.transform.position =new Vector3( this.transform.position.x, this.transform.position.y,2.5f);
			} else {
				ability.transform.position =new Vector3( 0, 0,2.5f);
			}

			ability.gameObject.SetActive (true);

			this.gameObject.SetActive (false);
	//		///////Debug.Log ("Col");
		}
	
	}
}

