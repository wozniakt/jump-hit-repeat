﻿using UnityEngine;
using System.Collections;

public class DisableAfterCollision : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void OnCollisionEnter2D (Collision2D col)
	{
		this.gameObject.SetActive (false);
	}
	void OnTriggerEnter2D (Collider2D col)
	{
		this.gameObject.SetActive (false);
	}
}

