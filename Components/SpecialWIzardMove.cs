﻿using UnityEngine;
using System.Collections;

public class SpecialWIzardMove : MonoBehaviour
{
//	public float  moveVelocityX,startMoveVelocityX, moveVelocityY,startMoveVelocityY,forceX,forceY;
//	public float timer;
	float  moveVelocityX,moveVelocityY;
	public Rigidbody2D rb2D;
	// Use this for initialization
	void OnEnable ()
	{
		//moveVelocityX = 0;
		rb2D = GetComponent<Rigidbody2D> ();
		StartCoroutine (moveSPecialWIzard ());
	}

	IEnumerator moveSPecialWIzard(){
		moveVelocityY = -2;
		yield return new WaitForSeconds (2);
		moveVelocityY = 2;
		yield return new WaitForSeconds (2);
		StartCoroutine (moveSPecialWIzard ());
	}
		
	void FixedUpdate(){
		rb2D.velocity = new Vector2 (moveVelocityX, moveVelocityY);
	}



}

