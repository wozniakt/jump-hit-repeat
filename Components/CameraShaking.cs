﻿using UnityEngine;
using System.Collections;

public class CameraShaking : MonoBehaviour
{
	GameObject mainCamera;
	public float shakesNumber, shakeBounds;
	Vector3 startingPos;

//
//	public delegate void GroundShaking();
//	public event GroundShaking eGroundShaking;
//
//	public void TriggerGroundShaking( ){
//		if (eGroundShaking!=null) {
//			eGroundShaking (); 
//		}
//	}
	void Start(){
		mainCamera = GameObject.Find ("Main Camera");
		startingPos = mainCamera.transform.position;
	}
	void OnEnable(){


//		eGroundShaking += this.shakeCamera;
	}
	void OnDisable(){
//		eGroundShaking -= this.shakeCamera;
	}
	// Use this for initialization
	IEnumerator shake ()
	{
		for (int i = 0; i < shakesNumber; i++) {
//			///////Debug.Log ("camera is shaking");
			mainCamera.transform.position = new Vector3 (startingPos.x + Random.Range (-shakeBounds, shakeBounds), startingPos.y + Random.Range (-shakeBounds, shakeBounds), startingPos.z);
			yield return new WaitForSeconds (0.1f);
		}
		mainCamera.transform.position = startingPos;
	}
	
	// Update is called once per frame
	public void shakeCamera ()
	{
		StartCoroutine (shake ());
	}
}

