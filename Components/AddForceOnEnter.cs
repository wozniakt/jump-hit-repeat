﻿using UnityEngine;
using System.Collections;

public class AddForceOnEnter : MonoBehaviour
{
	public float ForceX, ForceY;
	// Use this for initialization
	void OnEnable ()
	{
		GetComponent<Rigidbody2D> ().AddForce (new Vector2(ForceX, ForceY));
	}
	

}

