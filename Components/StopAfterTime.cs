﻿using UnityEngine;
using System.Collections;

public class StopAfterTime : MonoBehaviour
{
   public float timeToDisable;
	public float timer;
	Rigidbody2D rb2d;
	Collider2D col2d;
	// Use this for initialization
	void OnEnable ()
	{
		//StartCoroutine (stop ());
		rb2d=GetComponent<Rigidbody2D>();
		col2d =GetComponent<Collider2D> ();
		timer=0;
//		StartCoroutine (stop ());
	}
//	void Start ()
//	{
//		//StartCoroutine (stop ());
//		timer=0;
//	}
//
	void FixedUpdate(){
		timer =timer+Time.fixedDeltaTime;
		if (timeToDisable<=timer) {
			rb2d.velocity = Vector2.zero;
			col2d.enabled = false;
		}
	}
	// Update is called once per frame
//	IEnumerator stop ()
//	{
//		///////Debug.Log ("timeToDisable : " + timeToDisable);
//		yield return new WaitForSeconds (timeToDisable);
//		rb2d.velocity = Vector2.zero;
//		col2d.enabled = false;
//	}
	void OnDisable ()
	{
		col2d.enabled = true;
		StopAllCoroutines();

	}
}
