﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(DissapearEffect))]
[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(Collider2D))]

public class CollectItemEffect : MonoBehaviour
{
	public float dissaperaInterval, dissapearDelay;
	//Flickering flickering;
	public int CoinsAmount;
	GameObject coinCollector;
	bool moveTowardsCollector;
	Rigidbody2D rigidbody2d;
	DissapearEffect dissapering;

	void OnEnable(){
		rigidbody2d = GetComponent<Rigidbody2D> ();
		//flickering = GetComponent<Flickering> ();
		dissapering = GetComponent<DissapearEffect> ();
		moveTowardsCollector = true;
		coinCollector = GameObject.Find ("CoinCollector");
		//flickering.enabled = true;
		//this.gameObject.layer = 12;
	}


	void OnTriggerEnter2D(Collider2D other){ 

		if (other.transform.name== "CoinCollector") {
			SoundsManager.Instance.PlaySound (SoundType.CoinCollect);
			ManagerOfGlobalEvents.instance.TriggerOnGetPoints (10);
			moveTowardsCollector = true;
		//	flickering.enabled = false;
			//rigidbody2d.gravityScale = 1;
			this.gameObject.SetActive (false);

		}

	}

	void Update(){

		if (moveTowardsCollector==true) {
			transform.position = Vector2.MoveTowards (this.transform.position, coinCollector.transform.position, 22 * Time.deltaTime);
		}


	}
}

