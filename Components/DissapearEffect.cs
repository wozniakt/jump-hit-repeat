﻿using UnityEngine;
using System.Collections;

public class DissapearEffect : MonoBehaviour
{

	[HideInInspector]
	public Renderer renderer;
	public float DelayTime, IntervalTime;
	Vector2 scale;
	// Use this for initialization
	void OnEnable ()
	{
		//renderer =new Renderer();
		scale=GetComponent<Transform>().localScale;
		renderer = GetComponent<Renderer> ();
		StartCoroutine (fading (DelayTime, IntervalTime));
	}
	public void fade(float DelayTime, float IntervalTime){

		StartCoroutine (fading( DelayTime,  IntervalTime));
	}


	// Update is called once per frame
	public IEnumerator  fading (float DelayTime, float IntervalTime)
	{

		yield return new WaitForSeconds(DelayTime);
		for (float f = 2f; f >=0; f-= 0.07f) {
			/////////Debug.Log ("!!!" + f);

			scale.x = f*1;
			scale.y = f*1;
			transform.localScale =  new Vector2 (scale.x, scale.x);
			Color c = renderer.material.color;
			c.a = f;
			renderer.material.color = c;

			yield return new WaitForSeconds(IntervalTime);
		}
		/////////Debug.Log ("Fade out");


	}
	void OnDisable(){
		transform.localScale =  new Vector2 (1, 1);
		StopAllCoroutines ();
		Color c = renderer.material.color;
		c.a = 1;
		renderer.material.color = c;
	}
}

