﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroSpawner : MonoBehaviour
{
	public float interval;
	//public int heroNumber1,heroNumber2,heroNumber3,heroNumber4,heroNumber5;
	public List <int> heroesNumToSpawn;
	// Use this for initialization
	void Start ()
	{   
		StartCoroutine (SpawnHeroes ());
	
	}
	
	// Update is called once per frame
	public IEnumerator SpawnHeroes ()
	{
		foreach (var oHeroNumToSpawn in heroesNumToSpawn) {
			yield return new WaitForSeconds (interval);
			GameObject hero = PoolManager.instance.GetPooledObject_Hero (oHeroNumToSpawn);
			hero.transform.position =new Vector3( transform.position.x, transform.position.y,2.5f);
			hero.gameObject.SetActive (true);

//			///////Debug.Log (interval);
		}

//		if (DataManager.instance.oGameState!=GameState.GameLost) {
			StartCoroutine (SpawnHeroes ());
//		}
	

	}
}

