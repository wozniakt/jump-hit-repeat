﻿using UnityEngine;
using System.Collections;

public class ParallaxBackground : MonoBehaviour
{
	public float speedBG;
	Renderer renderer;
	Creature oCreature;
	float timer, tmpTimer;
	// Use this for initialization
	void Start ()
	{
		renderer = GetComponent<Renderer> ();
		oCreature = GameObject.FindGameObjectWithTag("Monster").GetComponent<CreatureInit>().oCreature;
	}
	
	// Update is called once per frame
	void Update ()
	{
		tmpTimer = Time.time;
		
		/////////Debug.Log ("timer:"+timer+" tmpTImer: "+tmpTimer);
		if (oCreature.CreatureState==CreatureState.Idle) {
			
			timer = Time.time;
		}
		if (oCreature.CreatureState!=CreatureState.Idle) {
			timer = timer;
		}
		Vector2 offset = new Vector2 (timer * speedBG, 0);
		renderer.material.mainTextureOffset = offset;
	}
}

