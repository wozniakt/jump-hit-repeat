﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PowerUpSpawner : MonoBehaviour
{
	public float interval;
	public int randomPowerUpNumber;
	public List<Ability> availableAbilities ;
	RecoilAfterHit recoil;
	// Use this for initialization
	void Start ()
	{ 	availableAbilities = new List<Ability> ();
		createAvailablePowerUpsList ();
		StartCoroutine (SpawnPowerUps ());
	}

	void createAvailablePowerUpsList(){
		
		foreach (Ability oAbility in DataManager.instance.oPlayerData.CurrentMonster.AbilitiesList) {
			if (oAbility.IsUnlocked) {
				availableAbilities.Add (oAbility);
			}
		}
	}

	// Update is called once per frame
	public IEnumerator SpawnPowerUps ()
	{
		yield return new WaitForSeconds (interval+(availableAbilities.Count*2));
		randomPowerUpNumber = (int)(Random.Range (0, availableAbilities.Count+1));
		GameObject powerUp = PoolManager.instance.GetPooledObject_PowerUp ("PowerUp"+randomPowerUpNumber);
//		///////Debug.Log (powerUp.name+"  "+availableAbilities.Count);
		powerUp.name = "PowerUp" + randomPowerUpNumber;
		powerUp.transform.position =new Vector3(transform.position.x, transform.position.y,2.5f);
		powerUp.gameObject.SetActive (true);
		//powerUp.GetComponent<RecoilAfterHit> ().RecoilX=Random.Range(powerUp.GetComponent<RecoilAfterHit> ().RecoilX-2,powerUp.GetComponent<RecoilAfterHit> ().RecoilX+2);
		//powerUp.GetComponent<RecoilAfterHit> ().Recoil ();
		powerUp.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-200,-120),0));
		StartCoroutine (SpawnPowerUps ());
	}
}

