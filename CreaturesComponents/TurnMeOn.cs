﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TurnMeOn : MonoBehaviour
{

	// Use this for initialization
	void OnEnable	 ()
	{
		ManagerOfGlobalEvents.instance.eBossOnScene += this.TurnOn;
		TurnOn(false);
	}

	// Update is called once per frame
	void OnDisable ()
	{
		ManagerOfGlobalEvents.instance.eBossOnScene -= this.TurnOn;
	}
	void TurnOn(bool switchOn){
		if (switchOn==true) {
			this.GetComponent<Image> ().color = Color.white;
		}
		if (switchOn==false) {
			this.GetComponent<Image> ().color = new Color(0,0,0,0);
		}
	}
}

