﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class MonsterSmash : MonoBehaviour
{
	//public float velocityJump;

	//Jump jump;
	ctrlMonsterShooting ctrlMonsterShoot;
	GroundHit groundHit;
	GameObject Monster;
	float jumpTimer;
	public float jumpMeasurementStart,jumpMeasurementStop;
	Rigidbody2D rigidbody2d;
	void OnEnable ()
	{
		Monster = GameObject.FindWithTag ("Monster");
	
	}

	public void ForceMeasureStart(){
		jumpMeasurementStart = Time.time;
	}

	public void ForceMeasureStop(){
		jumpMeasurementStop = Time.time;
		GameObject.FindWithTag ("Monster").GetComponent<GroundHit> ().JumpUp (jumpMeasurementStop-jumpMeasurementStart);

	}


}

