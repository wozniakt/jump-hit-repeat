﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
	public float  moveVelocityX,startMoveVelocityX, moveVelocityY,startMoveVelocityY,forceX,forceY;
	public float timer;
	public Rigidbody2D rb2D;
	// Use this for initialization
	void OnEnable ()
	{
		//moveVelocityX = 0;
		rb2D = GetComponent<Rigidbody2D> ();
		moveVelocityX = startMoveVelocityX;
		moveVelocityY = startMoveVelocityY;
	}

	void FixedUpdate(){
		rb2D.velocity = new Vector2 (moveVelocityX,moveVelocityY);
		rb2D.AddForce (new Vector2 (forceX, forceY), ForceMode2D.Impulse);

	}

	public void ChangeVelocity(float newVelocityX,float newVelocityY, float newforceX, float newforceY){
		moveVelocityX=newVelocityX;
		moveVelocityY=newVelocityY;
		forceX=newforceX;
		forceY=newforceY;
	}

//	public void Jump(float jumpX, float jumpY){
//		moveVelocityX = 0;
//		rb2D.AddForce (new Vector2(jumpX,jumpY),ForceMode2D.Impulse);
//	}
}

