﻿using UnityEngine;
using System.Collections;

public class MonsterStateManager : MonoBehaviour
{
	public GameObject powerUpParticle;
	ctrlMonsterShooting ctrlMonsterShoot;
	ctrlMonsterTakingDamage ctrlMonsterTakingDmg;
	CameraShaking cameraShaking;
	public HeroType heroType;
	public float walkSpeed, runSpeed, timeOfWalk, timeOfRun;
	float jumpMeasurementStart, jumpMeasurementStop, jumpVelocityX,  jumpVelocityY,landingVelocity,speedY;
	public float jumpMultipler,groundHitMultipler;
	Rigidbody2D rigidbody2d;
	CreatureState oMonsterState;
	Move move;
	MeleeAttack meleeAttack;
	Shot shot;
	Animator animator;
	Coroutine corWalkAndRun;
	Creature oCreature;
	int 		defaultLayer ;
	// Use this for initialization
	void OnEnable ()
	{
		oCreature = GetComponent<CreatureInit> ().oCreature;

		ctrlButtonForJump.instance.eJumpBtnPressDown += ForceMeasureStart;
		ctrlButtonForJump.instance.eJumpBtnPressUp += ForceMeasureStop;
		oCreature.eChangeCreatureState += this.changeState;



		rigidbody2d = GetComponent<Rigidbody2D> ();
		meleeAttack = GetComponent<MeleeAttack> ();
		shot = GetComponent<Shot> ();
		animator = GetComponent<Animator> ();
		cameraShaking = GetComponent<CameraShaking> ();
		ctrlMonsterShoot =  GetComponent<ctrlMonsterShooting> ();
		ctrlMonsterTakingDmg=GetComponent<ctrlMonsterTakingDamage> ();

		resetAnimatorStates ();
		defaultLayer = gameObject.layer;
	}
	void OnDisable(){
		ctrlButtonForJump.instance.eJumpBtnPressDown -= ForceMeasureStart;
		ctrlButtonForJump.instance.eJumpBtnPressUp -= ForceMeasureStop;
		oCreature.eChangeCreatureState -= this.changeState;
		StopAllCoroutines ();
	}


	// Update is called once per frame
	public void changeState(CreatureState monsterState)
	{
		resetAnimatorStates ();
		oCreature.CreatureState = monsterState;
		if (oCreature.CreatureState==CreatureState.IsPreparingToJump) {
			animator.SetBool("isPreparingToJump",true);
			SoundsManager.Instance.PlaySound (SoundType.JumpUp);
			 gameObject.layer=defaultLayer;
		}
		if (oCreature.CreatureState==CreatureState.JumpUp) {
			gameObject.layer=defaultLayer;
			animator.SetBool("isJumpUp",true);
		}
		if (oCreature.CreatureState==CreatureState.JumpDown) {
			animator.SetBool ("isJumpUp", false);
			gameObject.layer = 17;
		}

		if (oCreature.CreatureState==CreatureState.Hurt) {
//			///////Debug.Log ("SHOULD HUTR");
			animator.SetBool("isHurt",true);
			SoundsManager.Instance.PlaySound (SoundType.MonsterHit);
			gameObject.layer=defaultLayer;
		}

		if (oCreature.CreatureState==CreatureState.Dead) {
			animator.SetBool("isDead",true);
			gameObject.layer=defaultLayer;
		}
		if (oCreature.CreatureState==CreatureState.Idle) {
			animator.SetBool("isIdle",true);
			gameObject.layer=defaultLayer;
		}
		if (oCreature.CreatureState==CreatureState.PowerUpNow) {
			powerUpParticle.SetActive (true);
			oCreature.TriggerChangeCreatureState (CreatureState.Idle);
			gameObject.layer=17;
		}
	}
	public void ShotFireball(){
		shot.singleShot (BulletType.Fireball_Hero, -4, -1, 0,5, 5, 0, 1, 0);
	}

	void resetAnimatorStates(){
		animator.SetBool("isDead",false);
		animator.SetBool("isJumpUp",false);
		animator.SetBool("isPreparingToJump",false);
		animator.SetBool("isHurt",false);
		animator.SetBool("isIdle",false);
	}

	public void ForceMeasureStart(){
		if (this.oCreature.CreatureState==CreatureState.Idle) {
			jumpMeasurementStart = Time.time;
			oCreature.TriggerChangeCreatureState (CreatureState.IsPreparingToJump);
		}
	}

	public void ForceMeasureStop(){
		if (this.oCreature.CreatureState == CreatureState.IsPreparingToJump) {
			jumpMeasurementStop = Time.time;
			jumpVelocityY = jumpMeasurementStop - jumpMeasurementStart;
			JumpUp (0, jumpVelocityY); 
		}
	}

	public void JumpUp (float jumpVelocityX, float jumpVelocityY)
	{	
		
			oCreature.TriggerChangeCreatureState (CreatureState.JumpUp);
			animator.SetBool ("JumpUp", true);
			rigidbody2d.velocity = Vector2.zero;
			rigidbody2d.AddForce (new Vector2 (jumpVelocityX, jumpVelocityY * jumpMultipler), ForceMode2D.Impulse);
	
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (oCreature.CreatureState != CreatureState.Hurt && oCreature.CreatureState != CreatureState.Dead && oCreature.CreatureState == CreatureState.JumpDown) {
			landingVelocity = jumpVelocityY * jumpMultipler * groundHitMultipler;
			if (col.gameObject.name == "Ground" && (landingVelocity) > 10f && oCreature.CreatureState != CreatureState.Hurt) {
				SoundsManager.Instance.PlaySound (SoundType.Earthquake);
				cameraShaking.shakeCamera ();
				ctrlMonsterShoot.singleShoot (BulletType.EarthQuake, landingVelocity, 1);
					oCreature.TriggerChangeCreatureState (CreatureState.Idle);
				animator.SetBool ("isOnJump", false);
			} else if (col.gameObject.name == "Ground" && oCreature.CreatureState == CreatureState.HasBeenHit) {
				//	oCreature.TriggerChangeCreatureState (CreatureState.HitGround);
				oCreature.TriggerChangeCreatureState (CreatureState.Idle);
				landingVelocity =0;	
				animator.SetBool ("isOnJump", false);
			}
		} else {
			landingVelocity =0;	
			oCreature.TriggerChangeCreatureState (CreatureState.Idle);
		}	
	}

	public void Freeze(){
		if (oCreature.CreatureState!=CreatureState.Hurt && oCreature.CreatureState!=CreatureState.Dead) {
			StartCoroutine (FreezeInAir ());
		}

	}

	void OnCollisionStay2D (Collision2D col)
	{
		if (oCreature.CreatureState != CreatureState.Hurt && oCreature.CreatureState != CreatureState.Dead && oCreature.CreatureState == CreatureState.JumpDown) {
			oCreature.TriggerChangeCreatureState (CreatureState.Idle);
		}
	}

	IEnumerator FreezeInAir(){
		//oCreature.TriggerChangeCreatureState (CreatureState.JumpDown);
		rigidbody2d.isKinematic = true;
		yield return new WaitForSeconds (0.9f);
		rigidbody2d.isKinematic = false;
		rigidbody2d.AddForce(new Vector2(jumpVelocityX,-jumpVelocityY*jumpMultipler/2),ForceMode2D.Impulse);
		oCreature.TriggerChangeCreatureState (CreatureState.JumpDown);
	}

	public void	StopHurt(){
		oCreature.TriggerChangeCreatureState (CreatureState.Idle);
	}
}
