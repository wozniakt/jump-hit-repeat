﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour
{
	GameObject bulletGO;
	Rigidbody2D bulletRigidBody2D;
	// Use this for initialization
	public void singleShot(BulletType bulletType, float velocityX,float offsetX, float altitude, float disableMulitpler, float StopMultipler,float delayBetweenBullets, int rounds, float initialDelay){
		StartCoroutine (singleShots (bulletType,  velocityX, offsetX,  altitude,  disableMulitpler,  StopMultipler, delayBetweenBullets,  rounds, initialDelay));
	}

	public IEnumerator singleShots(BulletType bulletType, float velocityX,float offsetX, float altitude, float disableMulitpler, float StopMultipler, float delayBetweenBullets, int rounds, float initialDelay){
		yield return new WaitForSeconds (initialDelay);
		for (int i = 0; i < rounds; i++) {
	
		yield return new WaitForSeconds (delayBetweenBullets);
		bulletGO=PoolManager.instance.GetPooledObject_Bullet (bulletType);
			bulletRigidBody2D = bulletGO.GetComponent<Rigidbody2D> ();
			bulletRigidBody2D.GetComponent<DisableAfterTime> ().timeToDisable = disableMulitpler;
			bulletRigidBody2D.GetComponent<StopAfterTime> ().timeToDisable = StopMultipler;
		bulletGO.transform.position = new Vector3 (this.transform.position.x+offsetX, this.transform.position.y +altitude,2);

		bulletGO.SetActive (true);

		
		bulletRigidBody2D.velocity= ( new Vector2(velocityX,transform.position.y));
//		///////Debug.Log ("Probably shoot" +bulletGO.name+bulletGO.transform.position);
		

		}
	}

}

