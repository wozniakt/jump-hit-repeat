﻿using UnityEngine;
using System.Collections;

public class TriggerForUIbossBar : MonoBehaviour
{

	// Use this for initialization
	void OnEnable ()
	{
		ManagerOfGlobalEvents.instance.TriggerBossOnScene (true);
	}

	// Update is called once per frame
	void OnDisable ()
	{
		ManagerOfGlobalEvents.instance.TriggerBossOnScene (false);
	}
}

