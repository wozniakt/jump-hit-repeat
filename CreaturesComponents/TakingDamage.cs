﻿using UnityEngine;
using System.Collections;

public class TakingDamage : MonoBehaviour
{
	public float dieTime;
	Creature oCreature;
	// Use this for initialization
	Immunity immunity;

	void Start ()
	{
		oCreature = GetComponent<CreatureInit> ().oCreature;
		immunity = GetComponent<Immunity> ();
	}
	
	// Update is called once per frame
	public void LoseHp (int hp, Creature oCreature)
	{
//		///////Debug.Log ("lose HP:" + hp +"oCreature.Hp"+oCreature.Hp);	
		oCreature.Hp = oCreature.Hp - hp;
		if (oCreature.Hp>0) {
			immunity.BeImmune ();	
		}

		if (oCreature.Hp<=0) {

			StartCoroutine(die(dieTime));
		}
	}

	public IEnumerator die(float dieTime){
		if (this.CompareTag("Hero")==true) {
	
		ManagerOfGlobalEvents.instance.TriggerUpdateStoryText (StoryState.ShowLetters);
		GameObject explosion=  PoolManager.instance.GetPooledObject_Effect (EffectType.ExplosionDeath);
		explosion.transform.position = this.transform.position;
		explosion.SetActive (true);
		for (int i = 0; i < 1; i++) {
			GameObject coin=  PoolManager.instance.GetPooledObject_Effect (EffectType.Coin);
			coin.transform.position = this.transform.position;
			coin.SetActive (true);
			yield return new WaitForSeconds (0.001f);
		}
		oCreature.Hp = oCreature.MaxHp;
		yield return new WaitForSeconds (dieTime);
		this.gameObject.SetActive (false);

		}
	}
}

