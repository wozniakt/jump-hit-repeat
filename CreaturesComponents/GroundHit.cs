﻿using UnityEngine;
using System.Collections;

public class GroundHit : MonoBehaviour
{
	MonsterStateManager oMonsterStateManager;
	float landingVelocity;
	Rigidbody2D rigidbody2d;
	ctrlMonsterShooting ctrlMonsterShoot;
	ctrlMonsterTakingDamage ctrlMonsterTakingDmg;
	public float groundHitPower;
	CameraShaking cameraShaking;
	Creature oCreature;
	//bool isJumping;
	// Use this for initialization
	void Start ()
	{
		oCreature = GetComponent<CreatureInit> ().oCreature;
		cameraShaking = GetComponent<CameraShaking> ();
		oMonsterStateManager = GetComponent<MonsterStateManager> ();
		rigidbody2d = GetComponent<Rigidbody2D> ();
		ctrlMonsterShoot =  GetComponent<ctrlMonsterShooting> ();
		ctrlMonsterTakingDmg=GetComponent<ctrlMonsterTakingDamage> ();
	}


	void FixedUpdate(){
		if (rigidbody2d.velocity.y>0 ) {//&& isJumping==false
			/////////Debug.Log("Jump up shoud");
			oCreature.TriggerChangeCreatureState (CreatureState.JumpUp);

		}
		if (rigidbody2d.velocity.y<=0 && oCreature.CreatureState==CreatureState.JumpUp ) {//&& isJumping==true
			//isJumping=false;
			/////////Debug.Log("Freeze up shoud");
			StartCoroutine(	FreezeInAir ());
			oCreature.TriggerChangeCreatureState (CreatureState.JumpDown);
			//oCreature.CreatureState = CreatureState.JumpDown;
		}
	}
	IEnumerator FreezeInAir(){
		oCreature.TriggerChangeCreatureState (CreatureState.JumpDown);
		rigidbody2d.isKinematic = true;
		yield return new WaitForSeconds (0.5f);
		rigidbody2d.isKinematic = false;
		rigidbody2d.AddForce(new  Vector2(0,-groundHitPower*1000));
	}


	public void JumpUp(float jumpPower){ //groundHitPower=jumpPower
		/////////Debug.Log("Jump up shoud with jumppower="+jumpPower);
		oCreature.TriggerChangeCreatureState (CreatureState.JumpDown);
		rigidbody2d.AddForce(new Vector2(0,jumpPower)*1000);
		groundHitPower = jumpPower;
		//yield return new WaitForSeconds (0);
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		landingVelocity = col.relativeVelocity.magnitude;
		if (col.gameObject.name=="Ground" && (landingVelocity)>6f && oCreature.CreatureState!=CreatureState.HasBeenHit) {
		//	///////Debug.Log("Hit groumd");
			cameraShaking.shakeCamera ();
			ctrlMonsterShoot.singleShoot(BulletType.EarthQuake,landingVelocity*2f,1);
			//oCreature.TriggerChangeCreatureState (CreatureState.HitGround);
		} else if(col.gameObject.name=="Ground" && oCreature.CreatureState==CreatureState.HasBeenHit) {
			//oCreature.TriggerChangeCreatureState (CreatureState.HitGround);
		}
	}
}

