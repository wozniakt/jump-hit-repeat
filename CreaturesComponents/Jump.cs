﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour
{

	// Use this for initialization
	Rigidbody2D rigidbody2d;
	void Start(){
		rigidbody2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	public void JumpUp (float jumpVelocity)
	{
		rigidbody2d.AddForce(new Vector2(0,jumpVelocity));
	}
}

