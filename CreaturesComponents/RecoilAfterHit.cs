﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof(Flickering))]
public class RecoilAfterHit : MonoBehaviour
{
	public float RecoilX,RecoilY;
	Rigidbody2D rb2D;


	ctrlHeroStates ctrlHeroesState;
	Flickering flickering;
	// Use this for initialization
	void Start ()
	{
		ctrlHeroesState = GetComponent<ctrlHeroStates> ();
		flickering = GetComponent<Flickering> ();
		rb2D = GetComponent<Rigidbody2D> ();
	}
	void OnEnable ()
	{
		flickering = GetComponent<Flickering> ();
		rb2D = GetComponent<Rigidbody2D> ();
	}
	void OnDisable()
	{
		StopAllCoroutines ();
	}

	public void Recoil(){
		StartCoroutine (StartRecoil ());
	}
	// Update is called once per frame
	public IEnumerator StartRecoil ()
	{
		
		if (this.isActiveAndEnabled==true) {
//		
			GameObject effect=  PoolManager.instance.GetPooledObject_Effect (EffectType.HitWhite);
			effect.transform.position = this.transform.position;
			effect.SetActive (true);
			for (int i = 0; i < 24; i++) {
				rb2D.AddForce (new Vector2 (RecoilX,RecoilY-i*20));
//				///////Debug.Log ("RECOIL");
				yield return new WaitForSeconds (0.0001f);
			}
				
			//flickering.startFlickering ();

			yield return new WaitForSeconds (0.1f);

		}

	}
}

