﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossBarProgress : MonoBehaviour
{
	HeroSpawner BossSpawner;
	GameObject BossSpawnerGO;
	Image BossBarImg;
	float ProgressTimer;
	// Use this for initialization
	void Start ()
	{	
		ResetTimer ();
		BossSpawnerGO = GameObject.Find ("BossSpawner");
		BossSpawner = BossSpawnerGO.GetComponent<HeroSpawner> ();
		BossBarImg = this.GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update ()
	{

		ProgressTimer = ProgressTimer + Time.deltaTime;
		BossBarImg.fillAmount = ProgressTimer / BossSpawner.interval;
		if (BossBarImg.fillAmount>=1) {
			BossSpawner.interval = BossSpawner.interval * 2;
			ResetTimer ();
		}
	}
	void ResetTimer(){
		ProgressTimer = 0;
	}
	void OnEnable(){
		ResetTimer ();
	}
}

