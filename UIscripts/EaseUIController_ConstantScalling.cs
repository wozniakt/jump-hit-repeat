﻿using UnityEngine;
using System.Collections;
using EaseTools;
public class EaseUIController_ConstantScalling : MonoBehaviour
{
	public float waitTime;
	EaseUI easeUIComponent;
	bool shouldMoveIn;
	// Use this for initialization
	void Start ()
	{
		easeUIComponent = GetComponent<EaseUI> ();

		StartCoroutine (DelayCoroutine (waitTime));

	}
	void OnEnable(){
		easeUIComponent = GetComponent<EaseUI> ();
		StartCoroutine (DelayCoroutine (waitTime));
	}

	IEnumerator DelayCoroutine(float waitTime){
		yield return new WaitForSeconds (waitTime);
		StartCoroutine (EaseCoroutineInit ());
	}

	IEnumerator EaseCoroutineInit ()
	{	
		yield return new WaitForSeconds (waitTime);
		easeUIComponent.DurationSca = 0.1f;
		easeUIComponent.StartScale=(new Vector2(1,1));
		easeUIComponent.FinalScale=(new Vector2(1.2f,1.2f));
		easeUIComponent.ScaleIn ();
		yield return new WaitForSeconds (1);
		easeUIComponent.ScaleOut ();
		StartCoroutine (EaseCoroutineInit ());
	}



}