﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CanvasTextForSingleGO : MonoBehaviour
{
	Text text;
	float colorA;
	// Use this for initialization
//	void Awake ()
//	{
//		ManagerOfGlobalEvents.instance.eShowText += this.ShowText;
//		text = this.GetComponent<Text> ();
//	}
	void Start ()
	{
		ManagerOfGlobalEvents.instance.eShowText += this.ShowText;
		text = this.GetComponent<Text> ();

	}
	void OnDisable ()
	{
		ManagerOfGlobalEvents.instance.eShowText -= this.ShowText;


	}

	void ShowText (string textToShow, Vector2 pos)
	{
		StartCoroutine (showFadeText (textToShow, pos));
	}

	IEnumerator showFadeText(string textToShow, Vector3 pos){
		
		text.text = "+"+textToShow;

		//yield return new WaitForSeconds (2f);
		for (float i = 0; i < 1; i=i+0.03f) {
			text.transform.localPosition = new Vector3 (pos.x, pos.y+2+i*50,-2f);	
			yield return new WaitForSeconds (0.000001f);
			colorA = 1 - i;
			text.color = new Color (text.color.r, text.color.g, text.color.b, colorA);
		}
		text.text = "";
	}
}

