﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
public class StoryManager : MonoBehaviour
{
	int currentGoodChestID;
	List <string> storyTextsList=new List<string>();
	Chest currentGoodChest;
	public StoryState currentStoryState;
	public Text textBox;
	public int storyCounter;
	public string currentStoryText, myStoryText1;
	public static StoryManager instance;
	public List<ChestInit> ChestsInits=new List<ChestInit>();
	public List<Chest> Chests=new List<Chest>();
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}

	void OnDisable(){
		ManagerOfGlobalEvents.instance. eUpdateStoryText -= this.ShowNextLetter;
	}


	// Use this for initialization
	void Start ()
	{
		foreach (GameObject goChest in GameObject.FindGameObjectsWithTag ("Chest")) {
			ChestsInits.Add (goChest.GetComponent<ChestInit> ());
			Chests.Add (goChest.GetComponent<ChestInit> ().oChest);
			goChest.SetActive (false);
			/////////Debug.Log (goChest.GetComponent<ChestInit> ().oChest.ID + goChest.GetComponent<ChestInit> ().oChest.Color);
		}		 
		//randomizeChests ();
		/////////Debug.Log ("1 CHESTScount " + Chests.Count + " currentGOODID " + currentGoodChest.ID);


		ManagerOfGlobalEvents.instance. eUpdateStoryText += this.ShowNextLetter;

		//ShowChests (false);
	}
	void randomizeChests(){
	//	///////Debug.Log	("randomizeChests");
		currentGoodChestID = Random.Range (1, 4);
		foreach (Chest oChest in Chests) {
			if (oChest.ID==currentGoodChestID) {
				
				oChest.Gold = 100;
				oChest.Goblins = 0;
				currentGoodChest = oChest;
				///////Debug.Log	("currentGoodChest :  "+currentGoodChest.Color);
				/////////Debug.Log ("2 CHESTScount " + Chests.Count + " currentGOODID " + currentGoodChest.ID);
			} else {
				oChest.Gold = 0;
				oChest.Goblins = 5;			
			}	
		}
		randomizeStoryText (currentGoodChest.Color);
	}


	// Update is called once per frame
	void ShowNextLetter (StoryState storyState)
	{
	//	///////Debug.Log	("currentGoodChest.Color: "+currentGoodChest.Color+" myStoryText1: "+myStoryText1 );
		currentStoryState = storyState;
		if (storyCounter<myStoryText1.Length && currentStoryState==StoryState.ShowLetters) {
		storyCounter = storyCounter + 1;
		currentStoryText=myStoryText1.Substring(0,storyCounter);
		textBox.text = currentStoryText;
		//ShowChests (false);
		} 
		if (storyCounter >= myStoryText1.Length && currentStoryState == StoryState.ShowLetters) {
			currentStoryState = StoryState.ShowChests;
		}
		if(currentStoryState==StoryState.ShowChests){
			ShowChests (true);
			//storyCounter =0;
			currentStoryText="";
			textBox.text = currentStoryText;
		}

	}
		
	public void ShowChests (bool showChest)
	{
		foreach (ChestInit oChest in ChestsInits) {
			oChest.gameObject.SetActive (showChest);
		} 
		if (showChest==false) {
			//randomizeChests ();
		}
	}

	public void reInitStory(){
	//	///////Debug.Log ("reInitStory");
		currentStoryText = "";
		ShowChests (false);
		randomizeChests ();
		storyCounter =-1;
		ManagerOfGlobalEvents.instance.TriggerUpdateStoryText (StoryState.ShowLetters);
	}

	void randomizeStoryText(string chestColor){
		List <string> storyTextsList=new List<string>();

		storyTextsList.Add	("Gold is in "+chestColor+ " chest!");
		storyTextsList.Add	("Prize is waiting in "+chestColor+ " chest!");
		storyTextsList.Add	("Open "+ chestColor+ " chest!");
		storyTextsList.Add	("Money is in "+chestColor+" chest");
		storyTextsList.Add	("Choose "+chestColor+" treasury");
		storyTextsList.Add	("Gold is in crate painted with "+chestColor);
		storyTextsList.Add	("There is gold in "+chestColor+"crate");
		storyTextsList.Add	("Goblins are not hidden in "+chestColor + " coffer");
		storyTextsList.Add	("Money is waiting in " +chestColor+" ");
		storyTextsList.Add	("If you need gold, there is some in "+chestColor+" coffer");
		storyTextsList.Add	(chestColor + " chest contains gold!");
		storyTextsList.Add	("Gold, gold, gold in " +chestColor+ " crate");
		storyTextsList.Add	(chestColor+" chest has gold inside");
		storyTextsList.Add	("Pick the "+chestColor+" coffer" );
		storyTextsList.Add	("Choose crate which is "+chestColor);		
		storyTextsList.Add	("Chest with gold is colored "+chestColor);
		storyTextsList.Add	("Gold is only in "+chestColor+" chest");

//		foreach (var oStr in storyTextsList) {
//			///////Debug.Log (oStr);
//		}
	//	///////Debug.Log ("randomizeStoryText "+chestColor);
		myStoryText1 = storyTextsList [Random.Range (0, storyTextsList.Count - 1)];
//		///////Debug.Log ("randomizeStoryText "+myStoryText1+"    "+chestColor);
	}


}

