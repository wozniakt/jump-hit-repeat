﻿using UnityEngine;
using System.Collections;

public class ctrlButtonForJump : MonoBehaviour
{
	public static ctrlButtonForJump instance;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}

	// Use this for initialization
	public void PressDownJumpBtn ()
	{
		TriggerJumpBtnPressDown ();
	}
	public void PressUpJumpBtn ()
	{
		TriggerJumpBtnPressUp ();
	}
	
	public delegate void JumpBtnPressDown();
	public event JumpBtnPressDown eJumpBtnPressDown;

	public void TriggerJumpBtnPressDown(){
		if (eJumpBtnPressDown!=null) {
			eJumpBtnPressDown (); 
		}
	}

	public delegate void JumpBtnPressUp();
	public event JumpBtnPressUp eJumpBtnPressUp;

	public void TriggerJumpBtnPressUp(){
		if (eJumpBtnPressUp!=null) {
			eJumpBtnPressUp (); 
		}
	}
}

