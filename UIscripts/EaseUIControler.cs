﻿using UnityEngine;
using System.Collections;
using EaseTools;
public class EaseUIControler : MonoBehaviour
{
	 EaseUI easeUIComponent;
	bool shouldMoveIn;
	// Use this for initialization
	void Start ()
	{
		easeUIComponent = GetComponent<EaseUI> ();
		//easeUIComponent.MoveIn ();
		StartCoroutine (EaseCoroutineInit ());

	}
	void OnEnable(){
		easeUIComponent = GetComponent<EaseUI> ();
		StartCoroutine (EaseCoroutineInit ());
	}

	IEnumerator EaseCoroutineInit ()
	{	
		yield return new WaitForSeconds (0.1f);
		easeUIComponent.ScaleIn ();

	}
	void OnDisable(){
		transform.localScale = Vector2.zero;
	}


}

