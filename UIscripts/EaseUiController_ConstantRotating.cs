﻿using UnityEngine;
using System.Collections;
using EaseTools;
public class EaseUiController_ConstantRotating : MonoBehaviour
{
	EaseUI easeUIComponent;
	bool shouldMoveIn;
	// Use this for initialization
	void Start ()
	{
		easeUIComponent = GetComponent<EaseUI> ();

		StartCoroutine (EaseCoroutineInit ());

	}
	void OnEnable(){
		easeUIComponent = GetComponent<EaseUI> ();
		StartCoroutine (EaseCoroutineInit ());
	}

	IEnumerator EaseCoroutineInit ()
	{	
		yield return new WaitForSeconds (6f);
		easeUIComponent.RotateIn ();
		StartCoroutine (EaseCoroutineInit ());
	}



}