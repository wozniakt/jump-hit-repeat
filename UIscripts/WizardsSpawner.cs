﻿using UnityEngine;
using System.Collections;

public class WizardsSpawner : MonoBehaviour
{
	public float interval;
	// Use this for initialization
	void Start ()
	{
		StartCoroutine (SpawnWizards ());
	}

	// Update is called once per frame
	public IEnumerator SpawnWizards ()
	{
		GameObject hero = PoolManager.instance.GetPooledObject_Hero (2);
		hero.transform.position =new Vector3( transform.position.x, transform.position.y,2.5f);
		yield return new WaitForSeconds (interval);
		StartCoroutine (SpawnWizards ());

	}
}

