﻿using UnityEngine;
using System.Collections;

public class Explosions : MonoBehaviour
{

	// Use this for initialization
	void OnEnable ()
	{
		StartCoroutine (Explode ());
	}
	
	// Update is called once per frame
	IEnumerator Explode ()
	{
		GameObject effect=PoolManager.instance.GetPooledObject_Effect (EffectType.EarthExplosion);
		effect.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y-1,2);
		effect.SetActive (true);
		yield return new WaitForSeconds (0.002f);
		StartCoroutine (Explode ());
	}

	void OnDisable(){
		StopAllCoroutines();
	}
}

