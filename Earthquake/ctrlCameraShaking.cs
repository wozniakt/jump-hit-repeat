﻿using UnityEngine;
using System.Collections;

public class ctrlCameraShaking : MonoBehaviour
{
	CameraShaking cameraShaking;
	// Use this for initialization
	void OnEnable ()
	{
		cameraShaking = GetComponent<CameraShaking> ();
	}
	
	// Update is called once per frame
	void Start ()
	{
		cameraShaking.shakeCamera ();
	}
}

