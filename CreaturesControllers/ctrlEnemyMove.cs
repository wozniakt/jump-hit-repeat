﻿using UnityEngine;
using System.Collections;

public class ctrlEnemyMove : MonoBehaviour
{
	public string positionToAttack;
	TakingDamage takingDamage;
	RecoilAfterHit recoilAfterHit;
	Rigidbody2D rb2D;
	Move move;
	Shot shot;
	ctrlEnemyShooting ctrlEnemyShootin;
	// Use this for initialization
	void Start ()
	{
		move = GetComponent<Move> ();
		takingDamage = GetComponent<TakingDamage> ();
		recoilAfterHit = GetComponent<RecoilAfterHit> ();
		rb2D = GetComponent<Rigidbody2D> ();
		ctrlEnemyShootin = GetComponent<ctrlEnemyShooting> ();
	}

	IEnumerator OnTriggerEnter2D (Collider2D col)
	{

		if (col.gameObject.name==(positionToAttack)) {
			yield return new WaitForSeconds (0.001f);
			move.moveVelocityX = 0;
			//ctrlEnemyShootin.singleShoot (BulletType.Fireball, -1,2,30,60);
		}
	}

	IEnumerator OnTriggerExit2D (Collider2D col)
	{
		if (col.name==positionToAttack) {
			Debug.Log ("EXIT TRIGGR"+move.startMoveVelocityX+":::"+move.moveVelocityX);
			GetComponent<Move> ().timer = 0;
			yield return new WaitForSeconds (0.2f);
			GetComponent<Move> ().moveVelocityX = move.startMoveVelocityX;
			yield return new WaitForSeconds (0.01f);

		}
	}
}

