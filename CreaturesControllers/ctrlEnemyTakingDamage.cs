﻿using UnityEngine;
using System.Collections;

public class ctrlEnemyTakingDamage : MonoBehaviour
{
	TakingDamage takingDamage;
	RecoilAfterHit recoilAfterHit;
	Rigidbody2D rb2D;
	Move move;
	Creature oCreature;
	// Use this for initialization
	void Start ()
	{
		move = GetComponent<Move> ();
		takingDamage = GetComponent<TakingDamage> ();
		recoilAfterHit = GetComponent<RecoilAfterHit> ();
		rb2D = GetComponent<Rigidbody2D> ();
		oCreature = GetComponent<CreatureInit> ().oCreature;
	}

	
	// Update is called once per frame
	IEnumerator OnTriggerEnter2D (Collider2D col)
	{
		/////////Debug.Log ("!!!"+col.name);
		if (col.GetComponent<Power>()==true) {
			if (oCreature.Name=="Boss") {
				takingDamage.LoseHp (1, oCreature);	
			}
			takingDamage.LoseHp (col.gameObject.GetComponent<Power> ().power, oCreature);	
			rb2D.velocity = Vector2.zero;
			move.timer = 0;
			oCreature.TriggerChangeCreatureState (CreatureState.Hurt);
			recoilAfterHit.Recoil ();
			yield return new WaitForSeconds (1);

		}
	}
}

