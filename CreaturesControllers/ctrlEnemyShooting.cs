﻿using UnityEngine;
using System.Collections;

public class ctrlEnemyShooting : MonoBehaviour
{
	public float BulletSpeedX, BulletOffsetX,BulletALtitude, timeToStop, timeToDisable, delayBetweenShots, initialDelay;
	public int rounds;
	public string positionToAttack;
	Shot shot;
	public BulletType bullettype;
	// Use this for initialization
	void OnEnable 	 ()
	{
		//TODO: pobrać ten komponent wcześniej
		shot = GetComponent<Shot>();

	}


	IEnumerator OnTriggerEnter2D (Collider2D col)
	{
		if (col.gameObject.name==(positionToAttack)) {
			yield return new WaitForSeconds (1f);
			shot.singleShot (bullettype, BulletSpeedX,BulletOffsetX,BulletALtitude,timeToDisable,timeToStop, delayBetweenShots, rounds, initialDelay);
		}
	}


	// Update is called once per frame
//	public void singleShoot (BulletType bulletType,float  velocityX, float altitude, float disableMulitpler, float StopMultipler)
//	{
//		GetComponent<Shot>().singleShot (bulletType, velocityX,altitude,disableMulitpler,StopMultipler);
//	}
}


