﻿using UnityEngine;
using System.Collections;

public class ctrlMonsterTakingDamage : MonoBehaviour
{
	public bool monsterWasJustHit;
	TakingDamage takingDamage;
	RecoilAfterHit recoilAfterHit;
	Rigidbody2D rb2D;
	MonsterStateManager oMonsterStateManager;
	Creature oCreature;
	PlayerData oPlayerData;
	// Use this for initialization
	void Start ()
	{
		oCreature = GetComponent<CreatureInit> ().oCreature;
		oPlayerData=DataManager.instance.oPlayerData;
		oMonsterStateManager = GetComponent<MonsterStateManager> ();
		rb2D = GetComponent<Rigidbody2D> ();
		takingDamage = GetComponent<TakingDamage> ();
		recoilAfterHit = GetComponent<RecoilAfterHit> ();
	}

	// Update is called once per frame
	IEnumerator OnTriggerEnter2D (Collider2D col)
	{
		
		if (col.GetComponent<Power>()==true && DataManager.instance.oGameState==GameState.GameOn) {
			ManagerOfGlobalEvents.instance.TriggerOnLifeLoses (col.GetComponent<Power> ().power);
//			///////Debug.Log (this.name + "Just hited"+"oPlayerData.CurrentMonster.hp: "+ oPlayerData.CurrentMonster.Hp);
			monsterWasJustHit = true;
			GameObject explosion=  PoolManager.instance.GetPooledObject_Effect (EffectType.ExplosionHitMonster);
			explosion.transform.position = this.transform.position;
			explosion.SetActive (true);
			takingDamage.LoseHp (col.gameObject.GetComponent<Power> ().power,oPlayerData.CurrentMonster);	
			ManagerOfGlobalEvents.instance.TriggerUpdateData ();
			if (oPlayerData.CurrentMonster.Hp <=0) {rb2D.velocity = Vector2.zero;
				oCreature.TriggerChangeCreatureState (CreatureState.Dead);
				yield return new WaitForSeconds (2);
				ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameLost);

			}
			if (oPlayerData.CurrentMonster.Hp >0) {
				oCreature.TriggerChangeCreatureState (CreatureState.Hurt);
			}
		
			rb2D.velocity = Vector2.zero;
			//recoilAfterHit.Recoil ();
			yield return new WaitForSeconds (takingDamage.dieTime);
			monsterWasJustHit=false;
		}
	}
}
