﻿using UnityEngine;
using System.Collections;

public class ctrlHeroStates : MonoBehaviour
{
	public HeroType heroType;
	public float walkSpeed, runSpeed, timeOfWalk, timeOfRun, timeOfJump;
	 CreatureState oHeroState;
	Move move;
	MeleeAttack meleeAttack;
	Shot shot;
	Animator animator;
	Coroutine corWalkAndRun;
	Creature oCreature;
	// Use this for initialization
	void OnEnable ()
	{

		oCreature = GetComponent<CreatureInit> ().oCreature;
		oCreature.eChangeCreatureState += this.changeState;
		move = GetComponent<Move> ();
		corWalkAndRun = null;
		meleeAttack = GetComponent<MeleeAttack> ();
		shot = GetComponent<Shot> ();
		animator = GetComponent<Animator> ();
		oHeroState=new CreatureState();
	



		resetAnimatorStates ();
		//animator.SetBool("isStanding",true);
		//oHeroState = HeroState.Walk;
		if (this.name != ("SpecialWizard") ) {
			corWalkAndRun = StartCoroutine (WalkAndRun ());
		}
		if (this.name==("SpecialWizard") & this.heroType==HeroType.Wizard) {
			StartCoroutine (SpecialShooting ());
//			StartCoroutine(Floating());
		}
	}

	IEnumerator SpecialShooting ()
	{
		yield return new WaitForSeconds (4+Random.Range(0,4));
		oCreature.TriggerChangeCreatureState(CreatureState.MagicAttack);
		yield return new WaitForSeconds (1);
		oCreature.TriggerChangeCreatureState(CreatureState.Idle);
		StartCoroutine (SpecialShooting ());
	}
	public void startCorWalkAndRunAfterHit(){
		
		
		corWalkAndRun=StartCoroutine(WalkAndRun());

	}

	// Update is called once per frame
	public void changeState(CreatureState heroState)
	{
		resetAnimatorStates ();
		oCreature.CreatureState = heroState;
		if (oCreature.CreatureState==CreatureState.Walk) {
			animator.SetBool("isWalking",true);
		}
		if (oCreature.CreatureState==CreatureState.Run) {
			animator.SetBool("isRunning",true);
			SoundsManager.Instance.PlaySound (SoundType.Charge);
		}

		if (oCreature.CreatureState==CreatureState.JumpForward) {
			animator.SetBool("isJumping",true);

		}
		if (oCreature.CreatureState==CreatureState.Idle) {
			animator.SetBool("isStanding",true);
			if (corWalkAndRun!=null) {
				StopCoroutine (corWalkAndRun);
			}
		}
		if (oCreature.CreatureState==CreatureState.MeleeAttack) {
			move.ChangeVelocity (0,0,0,0);
			animator.SetBool("isAttacking",true);
			if (corWalkAndRun!=null) {
				StopCoroutine (corWalkAndRun);
			}

		}
		if (oCreature.CreatureState==CreatureState.MagicAttack) {
			move.ChangeVelocity (0,0,0,0);
			animator.SetBool("isAttacking",true);
			if (corWalkAndRun!=null) {
				StopCoroutine (corWalkAndRun);
			}
		}



		if (oCreature.CreatureState==CreatureState.Hurt) {
			if (corWalkAndRun!=null) {
				StopCoroutine (corWalkAndRun);
			}
			move.ChangeVelocity (0,0,0,0);
			animator.SetBool("isHurt",true);

		}
	}
	public void ShotFireball(){
		shot.singleShot (BulletType.Fireball_Hero, -4, -1, 0,5, 5, 0, 1, 0);
	}

	void resetAnimatorStates(){

		animator.SetBool("isRunning",false);
		animator.SetBool("isWalking",false);
		animator.SetBool("isStanding",false);
		animator.SetBool("isAttacking",false);
		animator.SetBool("isHurt",false);
		animator.SetBool("isJumping",false);
	}
//	void OnCollisionEnter2D(Collision2D col){
//		if (col.gameObject.name=="Ground") {
////			///////Debug.Log ("!!!");
//			corWalkAndRun=StartCoroutine(WalkAndRun());
//		}
//	}

	void OnTriggerEnter2D(Collider2D collider	)
	{
		
		if (collider.CompareTag("Monster")) {
			oCreature.TriggerChangeCreatureState(CreatureState.MeleeAttack);
		}
		if (collider.name=="Ground" && oCreature.CreatureState==CreatureState.Hurt) {
			//			///////Debug.Log ("!!!");
			corWalkAndRun=StartCoroutine(WalkAndRun());
		}

		if (collider.name==("PlaceForShooting") & this.heroType==HeroType.Wizard) {
			oCreature.TriggerChangeCreatureState(CreatureState.MagicAttack);
		}
	}


	IEnumerator WalkAndRun ()
	{
		move.ChangeVelocity (walkSpeed+Random.Range(-2.5f,0),0,0,0);
		oCreature.TriggerChangeCreatureState(CreatureState.Walk);
		yield return new WaitForSeconds (timeOfWalk);
		move.ChangeVelocity (runSpeed+Random.Range(-3,0),0,0,0);
		oCreature.TriggerChangeCreatureState(CreatureState.Run);
		yield return new WaitForSeconds (timeOfRun);
		//oCreature.TriggerChangeCreatureState(CreatureState.JumpForward);

		//move.ChangeVelocity (0, 0,-9,9);
		yield return new WaitForSeconds (0.2f);

		corWalkAndRun=StartCoroutine(WalkAndRun());

	}


	IEnumerator Floating ()
	{

		///////Debug.Log ("transform.position.y :  "+transform.position.y);
		move.ChangeVelocity (0,-2,0,0);
		if (transform.position.y<2) {
			move.ChangeVelocity (0,4,0,0);
		}
		yield return new WaitForSeconds (1f);
		move.ChangeVelocity (0,2,0,0);
		if (transform.position.y>7) {
			///////Debug.Log ("!!!");
			move.ChangeVelocity (0,-4,0,0);
		}
		yield return new WaitForSeconds (1f);
		StartCoroutine(Floating());
//
	}

//
//	public void JumpUp (float jumpVelocityX,float jumpVelocityY)
////	{	rigidbody2d.velocity = Vector2.zero;
////		rigidbody2d.AddForce(new Vector2(jumpVelocityX,jumpVelocityY));
//		//Recoil();
//	}

	void OnDisable(){
		oCreature.eChangeCreatureState -= this.changeState;
		StopAllCoroutines ();
	}
}

