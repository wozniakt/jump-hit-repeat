﻿using UnityEngine;
using System.Collections;

public class ctrlMonsterShooting : MonoBehaviour
{
	Shot shot;
	// Use this for initialization
	void OnEnable 	 ()
	{
		//TODO: pobrać ten komponent wcześniej
		shot = GetComponent<Shot>();

	}

	// Update is called once per frame
	public void singleShoot (BulletType bulletType,float  velocityX,float BulletOffsetX)
	{
		GetComponent<Shot>().singleShot (bulletType, velocityX,BulletOffsetX,0,velocityX/60,velocityX/60,0f,1,0);
	}

	public void specialShot_Flame ()
	{
		GetComponent<Shot>().singleShot (BulletType.Flame_Monster, 12f,0,0,2,2,0.1f,10,2);
	}

	public void specialShot_Flames ()
	{
		//for (int i = 0; i < 20; i++) {
		GetComponent<CreatureInit>().oCreature.TriggerChangeCreatureState(CreatureState.PowerUpNow);
		GetComponent<Shot>().singleShot (BulletType.Flame_Monster,0f,0,0,2,2,0.1f,10,2);
			//yield return new WaitForSeconds (0.1f);
		//}

	}
}

