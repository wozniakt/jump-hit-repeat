﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Chest 
{



	[SerializeField]
	int gold;
	public int Gold {
		get
		{ return gold; }
		set {

			gold = value;
		}
	}

	[SerializeField]
	int id;
	public int ID {
		get
		{ return id; }
		set {

			id = value;
		}
	}

	[SerializeField]
	int goblins;
	public int Goblins {
		get
		{ return goblins; }
		set {

			goblins = value;

		}
	}

	[SerializeField]
	int specialItem;
	public int SpecialItem {
		get
		{ return specialItem; }
		set {
			specialItem = value;
		}
	}
	[SerializeField]
	string color;
	public string Color {
		get
		{ return color; }
		set {
			color = value;
		}
	}




}