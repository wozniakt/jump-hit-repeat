﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Ability 
{


	public void BuyAbility(PlayerData oPlayerData){
		//patrz notatnik
		//foreach (Ability oAbility in abilitiesList) {
			if (oPlayerData.CoinsCount>=Cost && IsUnlocked==false) {
				//DataManager.instance.oPlayerData.MonstersList [this.Id - 1].AbilitiesList[this.Id-1].IsUnlocked = true;
			this.IsUnlocked=true;
				DataManager.instance.oPlayerData.CoinsCount = oPlayerData.CoinsCount -this.Cost;
				UI_Manager.instance.UpdateHudTexts ();
				//	///////Debug.Log (DataManager.instance.oPlayerData.CoinsCount);
			}	
		//}

		DataManager.instance.saveData ();
	}


	[SerializeField]
	int id;
	public int Id {
		get
		{ return id; }
		set {

			id = value;

		}
	}

	[SerializeField]
	AbilityType abilityType;
	public AbilityType AbilityType {
		get
		{ return abilityType; }
		set {

			abilityType = value;

		}
	}

	[SerializeField]
	int cost;
	public int Cost {
		get
		{ return cost; }
		set {

			cost = value;

		}
	}

	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked {
		get
		{ return isUnlocked; }
		set {

			isUnlocked = value;

		}
	}

}