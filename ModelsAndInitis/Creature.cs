﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Creature 
{

	public delegate void ChangeCreatureState(CreatureState creatureState);
	public event ChangeCreatureState eChangeCreatureState;

	public void TriggerChangeCreatureState(CreatureState creatureState){
		if (eChangeCreatureState!=null) {
			eChangeCreatureState (creatureState); 
		}
	}



	public void Buy(PlayerData oPlayerData){
		//patrz notatnik
		if (oPlayerData.CoinsCount>=Cost && isUnlocked==false) {
			DataManager.instance.oPlayerData.MonstersList [this.Id - 1].isUnlocked = true;
			isUnlocked = true;
			DataManager.instance.oPlayerData.CoinsCount = oPlayerData.CoinsCount - Cost;
			UI_Manager.instance.UpdateHudTexts ();
			//	///////Debug.Log (DataManager.instance.oPlayerData.CoinsCount);
		}
		DataManager.instance.saveData ();
	}

	[SerializeField]
	CreatureState creatureState;
	public CreatureState CreatureState{
		get
		{return creatureState;}
		set{
			creatureState = value; 
		}
	}

	[SerializeField]
	List<Ability> abilitiesList;
	public List<Ability> AbilitiesList{
		get
		{return abilitiesList;}
		set{
			abilitiesList = value;  //new List<Creature> (DataManager.instance.heroesList);
			//		coinsCount = DataManager.instance.coins;
		}
	}

	[SerializeField]
	int cost;
	public int Cost {
		get
		{ return cost; }
		set {

			cost = value;

		}
	}


	[SerializeField]
	int id;
	public int Id {
		get
		{ return id; }
		set {

			id = value;

		}
	}

	[SerializeField]
	int hp;
	public int Hp {
		get
		{ return hp; }
		set {
			TriggerChangeCreatureState(CreatureState.Hurt);
			hp = value;

		}
	}



		[SerializeField]
		int maxHp;
	public int MaxHp {
		get
			{ return maxHp; }
		set {

			maxHp = value;

		}
	}


	[SerializeField]
	bool isUnlocked;
	public bool IsUnlocked {
		get
		{ return isUnlocked; }
		set {

			isUnlocked = value;

		}
	}

	[SerializeField]
	string name;
	public string Name {
		get
		{ return name; }
		set {

			name = value;

		}
	}

	[SerializeField]
	float groundHitPower;
	public float GroundHitPower {
		get
		{ return groundHitPower; }
		set {

			groundHitPower = value;

		}
	}
	[SerializeField]
	int gold;
	public int Gold {
		get
		{ return gold; }
		set {

			gold = value;

		}
	}
}