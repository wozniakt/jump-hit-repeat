﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class UI_Manager : MonoBehaviour
{
	ManagerOfGlobalEvents managerOfGlobEvents;

	public Button Button_Jump;
	public Button ButtonMonsters_BackToMenu, ButtonMonsters_BackToMenu1, ButtonMonsters_BackToMenu2;
	public Button ButtonMenu_SoundOff,ButtonMenu_MusicOff,ButtonMenu_Start;
	public Button  ButtonPause_Resume,ButtonPause_Pause, ButtonPause_RestartGame,ButtonPause_BackToMenu,
	Button_BackToMenuAfterLost,
	ButtonMenu_MonstersPanel, ButtonMonsters_GoToMenu;
	public Button  ButtonQuick_Restart;
	public Button ButtonMenu_WatchAdd, ButtonMenu_Exit;
	public GameObject Menu_Title, PanelHud;
	public GameObject PanelQuick_Restart, PanelPause, PanelMenu, PanelBackground, PanelMonsters;
	public GameObject BossBar;
	public GameObject UIPanelMonsters_Content;
	
	public static string PREFAB_PATH = "Prefabs/";
	public Text coinsText, pointsText,highscoreText,FunnyEventDescriptionText;
	public Text comboText,achievementText;
	public GameObject GameBackground, GameOverTextAsImage;
	public  Image imgHp, imgAchievement,img_RandomEvent, ImageEvent_Visualization;
	public GameObject joystick;



	public static UI_Manager instance;
	Animator Img_RandomEvent_Animator,PanelBackground_Animator;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
	}

//	public  void OnPointerDown_( )
//	{
//		///////Debug.Log( "OnPointerDown called." );
//			
//	}
//	public  void OnPointerDown(PointerEventData data )
//	{
//		///////Debug.Log( "OnPointerDown called." );
//
//	}
//	public  void OnPointerUp( PointerEventData data )
//	{
//		///////Debug.Log( "OnPointerDown called." );
//
//	}


//	public void generatePrefabsUIhero(){
//		foreach (Creature oCreature in DataManager.instance.oPlayerData.MonstersList) {
//			GameObject obj = (GameObject)Instantiate(Resources.Load("Prefabs/Prefabs_UI/MonsterPageUI"));
//			obj.transform.SetParent ( UIPanelMonsters_Content.transform,false);
//			obj.name=obj.name.Replace ("(Clone)", oCreature.Id.ToString());
//		}
//	}


	public void generateCreaturesUI(){

		foreach (Creature oCreature in DataManager.instance.oPlayerData.MonstersList) {
			Creature TempCreature = oCreature;
			GameObject goCreatureUI = UIPanelMonsters_Content.transform.Find ("MonsterPageUI" + oCreature.Id).gameObject;
			Button btnCreatureBuy = goCreatureUI.transform.Find ("Monster").Find("ButtonBuy").GetComponent<Button> ();
			// Text txtBuy = btnCreatureBuy.transform.Find ("TextBuy").GetComponent<Text> ();
			// txtBuy = btnCreatureBuy.transform.Find ("ImageBuy").GetComponent<Image>();
			btnCreatureBuy.gameObject.SetActive(false);
			Text txtPriceCreature = btnCreatureBuy.transform.Find ("TextCost").GetComponent<Text> ();
			txtPriceCreature.text = "";
			Text txtNameCreature = goCreatureUI.transform.Find ("Monster").transform.Find ("TextName").GetComponent<Text> ();
			txtNameCreature.text = TempCreature.Name;
			Image imgMonster = goCreatureUI.GetComponent<Image> ();
			refreshColorUiElement (btnCreatureBuy, TempCreature.IsUnlocked);
			btnCreatureBuy.enabled = !TempCreature.IsUnlocked;
			btnCreatureBuy.onClick.RemoveAllListeners ();
			btnCreatureBuy.onClick.AddListener (() => {
				TempCreature.Buy (DataManager.instance.oPlayerData);
				generateCreaturesUI ();//przekaza w parametrze oPlayerData
			});
			if (!TempCreature.IsUnlocked) {
				txtPriceCreature.text = oCreature.Cost.ToString ();
				btnCreatureBuy.gameObject.SetActive(true);
			}

			foreach (Ability oAbility in oCreature.AbilitiesList) {
				Ability TempAbility = oAbility;
				GameObject goAbilityUI = UIPanelMonsters_Content.transform.Find ("MonsterPageUI" + oCreature.Id).gameObject;
				Button btnAbilityBuy= goCreatureUI.transform.Find ("ButtonBuyAbility"+oAbility.Id).GetComponent<Button>();
				Text txtPriceAbility= btnAbilityBuy.transform.Find ("TextCost").GetComponent<Text> ();
				txtPriceAbility.text = "";
				Text txtNameAbility= btnAbilityBuy.transform.Find ("TextName").GetComponent<Text> ();
				txtNameAbility.text= TempAbility.AbilityType.ToString ();
				txtNameAbility.alignment= TextAnchor.MiddleCenter;
				Image img = goCreatureUI.GetComponent<Image> ();
				refreshColorUiElement (btnAbilityBuy, TempAbility.IsUnlocked);
				btnAbilityBuy.enabled = !TempAbility.IsUnlocked;
				btnAbilityBuy.onClick.RemoveAllListeners ();
				btnAbilityBuy.onClick.AddListener (() => {
					TempAbility.BuyAbility(DataManager.instance.oPlayerData	);
					generateCreaturesUI();//przekaza w parametrze oPlayerData
				});
				if (!TempAbility.IsUnlocked) {
					txtPriceAbility.text = oAbility.Cost.ToString();
					txtNameAbility.alignment= TextAnchor.MiddleLeft;
				}}
		}}
	//}



	void Start ()
	{	managerOfGlobEvents = ManagerOfGlobalEvents.instance;
		//generatePrefabsUIhero ();
		generateCreaturesUI();
		managerOfGlobEvents.eUpdateData += this.UpdateHudTexts;
		managerOfGlobEvents.OnChangeGameState += this.UIChangeGameState;
//		managerOfGlobEvents.eUpdateData += this.UpdateHpText;

		//createScene ();

		AddListenersToButtons ();

		managerOfGlobEvents.TriggerOnChangeGameState (GameState.Menu);
		managerOfGlobEvents.TriggerUpdateData ();
	}



	public void ChangeBackground(string spriteName){

		Sprite newSprite =(Resources.Load<Sprite> ("Prefabs/Events_Prefabs/Event_"  + spriteName));
		GameBackground.GetComponent<SpriteRenderer> ().sprite = newSprite;
	}

	public void  UpdateHudTexts(){
		//odkmoentować gdy pojawi się HUD
		coinsText.text = "COINS: "+DataManager.instance.oPlayerData.CoinsCount;
		imgHp.fillAmount = (DataManager.instance.oPlayerData.CurrentMonster.Hp/10f);
//		pointsText.text = "POINTS: " + DataManager.instance.oPlayerData.PointsCount;
//		highscoreText.text="HIGHSCORE: "+DataManager.instance.oPlayerData.HighscorePointsCount;
	}
		
	 void  Update(){
//		///////Debug.Log (Time.timeScale);

	}
		

	void AddListenersToButtons(){
		// spoko, ale mozna tez na scenie, ale tak chyba nawet lepiej,
		//bo wiesz gdzie masz te przypisania a na scenie czasami zniknie referncja
//		Button_Jump.onClick.RemoveAllListeners ();
//		Button_Jump.OnPointerDown (() => OnPointerDown_());


		ButtonMenu_Start.onClick.RemoveAllListeners ();
		ButtonMenu_Start.onClick.AddListener (() => DataManager.instance.ReStartGame ());

		ButtonMenu_MonstersPanel.onClick.RemoveAllListeners ();
		ButtonMenu_MonstersPanel.onClick.AddListener (() => ShowMonstersPanel (true));

		ButtonMonsters_BackToMenu.onClick.RemoveAllListeners ();
		ButtonMonsters_BackToMenu.onClick.AddListener (() => ShowMonstersPanel (false));
		ButtonMonsters_BackToMenu1.onClick.RemoveAllListeners ();
		ButtonMonsters_BackToMenu1.onClick.AddListener (() => ShowMonstersPanel (false));
		ButtonMonsters_BackToMenu2.onClick.RemoveAllListeners ();
		ButtonMonsters_BackToMenu2.onClick.AddListener (() => ShowMonstersPanel (false));

		ButtonQuick_Restart.onClick.RemoveAllListeners ();
		ButtonQuick_Restart.onClick.AddListener (() => DataManager.instance.ReStartGame ());
//
		ButtonPause_Resume.onClick.RemoveAllListeners ();
		ButtonPause_Resume.onClick.AddListener (() => DataManager.instance.ResumeGame ());
//
		ButtonPause_Pause.onClick.RemoveAllListeners ();
		ButtonPause_Pause.onClick.AddListener (() => DataManager.instance.PauseGame ());
//
		ButtonPause_RestartGame.onClick.RemoveAllListeners ();
		ButtonPause_RestartGame.onClick.AddListener (() => DataManager.instance.ReStartGame ());

		ButtonPause_BackToMenu.onClick.RemoveAllListeners ();
		ButtonPause_BackToMenu.onClick.AddListener (() => DataManager.instance.BackToMenu ());

		ButtonMenu_Exit.onClick.RemoveAllListeners ();
		ButtonMenu_Exit.onClick.AddListener (() => DataManager.instance.ExitGame());

		Button_BackToMenuAfterLost.onClick.RemoveAllListeners ();
		Button_BackToMenuAfterLost.onClick.AddListener (() => DataManager.instance.BackToMenu ());

		ButtonMenu_SoundOff.onClick.RemoveAllListeners ();
		ButtonMenu_SoundOff.onClick.AddListener (() => SoundsManager.Instance.MuteAll());

		ButtonMenu_MusicOff.onClick.RemoveAllListeners ();
		ButtonMenu_MusicOff.onClick.AddListener (() => MusicManager.instance.MuteMusic());
	}
	public void  UIChangeGameState (GameState gameState)
	{
		PanelMonsters.SetActive (false);
		PanelMenu.SetActive (false);
		PanelPause.SetActive (false);
		PanelQuick_Restart.SetActive (false);
		PanelHud.SetActive (false);
		//BossBar.SetActive (false);
		//ButtonQuick_Restart.gameObject.SetActive (false);
		PanelQuick_Restart.SetActive (false);
		ButtonPause_Pause.enabled = false;
//		joystick.SetActive (false);
		//DataManager.instance.oGameState = gameState;
		switch (gameState) {
		case GameState.GameOn:
			
			ManagerOfGlobalEvents.instance.TriggerUpdateData ();
			ButtonPause_Pause.enabled = true;
//			joystick.SetActive (true);
			managerOfGlobEvents.TriggerUpdateData ();
			ButtonMenu_Start.transform.localScale = Vector3.zero;
		//	ButtonMenu_WatchAdd.transform.localScale = Vector3.zero;
			Menu_Title.transform.localScale = Vector3.zero;
//			ButtonMenu_MusicOff.transform.localScale = Vector3.zero;
//			ButtonMenu_SoundOff.transform.localScale = Vector3.zero;
			PanelMenu.SetActive (false);
			PanelHud.SetActive (true);
			BossBar.SetActive (true);
//			///////Debug.Log("BOSS BAR SET AC");
			break;
		case GameState.Menu:
			StopCoroutine("finishGame" );
			managerOfGlobEvents.TriggerUpdateData ();
			PanelMenu.SetActive (true);
			PanelHud.SetActive (false);
			UI_Manager.instance.StopCoroutine("finishGame" );
			break;
		case GameState.GamePause:
			PanelHud.SetActive (true);
			PanelPause.SetActive (true);
			break;
		case GameState.GameResume:
			PanelHud.SetActive (true);
			PanelPause.SetActive (false);
			ButtonPause_Pause.enabled = true;
			break;

		case GameState.GameLost:
			//yield return new WaitForSeconds (1);
			//	GameOver_prefab.SetActive (true);
			PanelQuick_Restart.SetActive (true);
			//StartCoroutine (finishGame ());
			break;

		}
	}
	public IEnumerator finishGame ()
	{
		
		ButtonQuick_Restart.gameObject.SetActive (true);
		//GameOver_prefab.transform.position = GameObject.Find ("PlaceHolder_GameOver").transform.position;

		yield return new WaitForSeconds (5f);
		if (DataManager.instance.oGameState != GameState.Menu) {
			managerOfGlobEvents.TriggerOnChangeGameState (GameState.Menu);
		}
			
	}

	public void ShowMonstersPanel(bool isVisible){	
		PanelMonsters.SetActive (isVisible);
	}


	public void  refreshColorUiElement(Button goUiElement, bool shouldBeVisible){
		Color colorUIElement = goUiElement.GetComponent<Image> ().color;
		if (shouldBeVisible) {
			//	///////Debug.Log ("I MADE IT inVISIBLE"+goUiElement.name);
			colorUIElement.r = 1;
			colorUIElement.g = 1;
			colorUIElement.b = 1;
			//colorUIElement.a = 0.5f;
		}
		if (!shouldBeVisible) {
			//	///////Debug.Log ("I MADE IT VISIBLE"+goUiElement.name);
			colorUIElement.r = 0.5f;
			colorUIElement.g = 0.5f;
			colorUIElement.b = 0.5f;
			//colorUIElement.a = 0.5f;
		}
		goUiElement.GetComponent<Image> ().color = colorUIElement;
	}



}



