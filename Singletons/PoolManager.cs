﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

	public static string POOLED_PREFAB_PATH = "Prefabs/";
	public static PoolManager instance;
	public GameObject HeroesPool, EffectsPool, BulletsPool, PowerUpsPool;
	public bool WillGrow = true;


	List<GameObject> pooledHeroes;
	List<GameObject> pooledEffects;
	List<GameObject> pooledBullets;
	List<GameObject> pooledPowerUps;

	void Awake()
	{  
		instance = this;
		pooledHeroes = new List<GameObject>();
		pooledEffects = new List<GameObject>();
		pooledBullets = new List<GameObject>();
		pooledPowerUps = new List<GameObject>();
		///////Debug.Log ("heroNumber");
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 3; j++) {
				createHeroes(j,i);
			}
		}
		for (int i = 20; i < 26; i++) {
			for (int j = 1; j < 3; j++) {
				createHeroes(j,i);
			}
		}
		for (int i = 30; i < 35; i++) {
			for (int j = 1; j < 3; j++) {
				createHeroes(j,i);
			}
		}


		createEffects(28,EffectType.EarthExplosion);
		createEffects(3,EffectType.SmokeWhite);
		createEffects(3,EffectType.HitWhite);
		createEffects(3,EffectType.Coin);
		createBullets (3, BulletType.Fireball_Hero);
		createBullets (3, BulletType.Flame_Monster);
		createBullets (2, BulletType.EarthQuake);

	//	createPowerUps(3,1);
	//	createEffects(3,1);
	//	createBullets(3,1);

	}
	//************************Heroes
	void createHeroes(int count, int heroNumber)
	{
		for (int i = 0; i < count; i++)
		{
			///////Debug.Log (heroNumber);
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Heroes/Hero"+heroNumber.ToString()));

			obj.SetActive(false);
			obj.name = "Hero"+heroNumber.ToString();
			pooledHeroes.Add(obj);
			obj.transform.SetParent(HeroesPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_Hero(int heroNumber)
	{
		foreach (GameObject item in pooledHeroes)
		{
			/////////Debug.Log ("1 Hero"+heroNumber.ToString());
			if (item.name == "Hero"+heroNumber.ToString())
			{     /////////Debug.Log ("2 Hero"+heroNumber.ToString());
				if (!item.activeInHierarchy)
				{
					/////////Debug.Log ("return item");
					return item;
				} 
			}
		}

		if (WillGrow)
		{
			/////////Debug.Log ("Will grow hero");
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Heroes/Hero"+ heroNumber.ToString()));
			obj.name="Hero"+heroNumber.ToString();
			pooledHeroes.Add(obj);
			obj.transform.SetParent(HeroesPool.transform);
			return obj;
		}
		return null;
	}
	//************************Effects
	void createEffects(int count, EffectType effectType)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+effectType.ToString()));
			obj.SetActive(false);
			obj.name = effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_Effect(EffectType effectType)
	{
		foreach (GameObject item in pooledEffects)
		{
			if (item.name == effectType.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+ effectType.ToString()));
			obj.name=effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
			return obj;
		}
		return null;
	}



//************************Bullets
void createBullets(int count, BulletType bulletType)
{
	for (int i = 0; i < count; i++)
	{
	GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Bullets/"+bulletType.ToString()));
		obj.SetActive(false);
		obj.name = bulletType.ToString();
		pooledBullets.Add(obj);
		obj.transform.SetParent(BulletsPool.transform);
	}

}

//
	public GameObject GetPooledObject_Bullet(BulletType bulletType)
{
		foreach (GameObject item in pooledBullets)
	{
		if (item.name == bulletType.ToString())
		{     
			if (!item.activeInHierarchy)
			{
				return item;
			} else
			{

			}   
		}
	}

	if (WillGrow)
	{
		GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Bullets/"+bulletType.ToString()));
		obj.name=bulletType.ToString();
		pooledBullets.Add(obj);
		obj.transform.SetParent(BulletsPool.transform);
		return obj;
	}
	return null;
}

//************************PowerUps
	void createPowerUps(int count, string powerUpName)
{
	for (int i = 0; i < count; i++)
	{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString()));
		obj.SetActive(false);
			obj.name = powerUpName.ToString();
		pooledPowerUps.Add(obj);
		obj.transform.SetParent(PowerUpsPool.transform);
	}

}
		
	public GameObject GetPooledObject_PowerUp(string powerUpName)
{
	foreach (GameObject item in pooledPowerUps)
	{
			if (item.name == powerUpName.ToString())
		{     
			if (!item.activeInHierarchy)
			{
				return item;
			} else
			{

			}   
		}
	}

	if (WillGrow)
	{
//			///////Debug.Log (powerUpTypeNumber);
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString()));
			obj.name=powerUpName.ToString();
		pooledPowerUps.Add(obj);
		obj.transform.SetParent(PowerUpsPool.transform);
		return obj;
	}
	return null;
}

}







