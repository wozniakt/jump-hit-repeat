﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
	string SpritesPath="Sprites/Backgrounds_sprites/";
	string ScenePath="Prefabs/Prefabs_Other/";
	string MonsterPath="Prefabs/Prefabs_Monsters/";
	public GameObject BackgroundPlaceHolder;
	public static GameManager instance;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
	}


	public void createScene(string backGroundName){
		
		GameObject sceneGamePlay =(GameObject)Instantiate(Resources.Load (ScenePath+"SceneGamePlay"));
		GameObject scenePlaceHolder = GameObject.Find ("ScenePlaceholder");
		sceneGamePlay.transform.SetParent(scenePlaceHolder.transform);
		sceneGamePlay.transform.localPosition = Vector2.zero;
///		Debug.Log (DataManager.instance.oPlayerData.CurrentMonster.Id +"   "+ DataManager.instance.oPlayerData.MonstersList [0].Id + "!!!");

//		if (DataManager.instance.oPlayerData.CurrentMonster==null) {
//			ManagerOfGlobalEvents.instance.TriggerChangeMonster (DataManager.instance.oPlayerData.MonstersList [0]);
//		}
//		if (DataManager.instance.oPlayerData.CurrentMonster.Id==null) {
//			ManagerOfGlobalEvents.instance.TriggerChangeMonster (DataManager.instance.oPlayerData.MonstersList [0]);
//		}
		GameObject monster = (GameObject)Instantiate(Resources.Load(MonsterPath +"Monster"+DataManager.instance.oPlayerData.CurrentMonster.Id));
		monster.transform.SetParent ( GameObject.Find ("MonsterPlaceHolder").transform);
		monster.transform.position= GameObject.Find ("MonsterPlaceHolder").transform.position;
		//DataManager.instance.oPlayerData.CurrentMonster = monster.GetComponent<CreatureInit> ().oCreature;
		BackgroundLoad (backGroundName);
	}


	public void BackgroundLoad(string spriteName){
		

		GameObject backgroundPlaceHolder =GameObject.Find ("BackgroundPlaceHolder");
		Sprite newSprite =(Resources.Load<Sprite> (SpritesPath  + spriteName));
		backgroundPlaceHolder.GetComponent<SpriteRenderer> ().sprite = newSprite;

	}

}
//public void generatePrefabsUIMonsters(){
//	foreach (Creature oCreature in DataManager.instance.oPlayerData.HeroesList) {
//		GameObject obj = (GameObject)Instantiate(Resources.Load("Prefabs/UI/HeroUI"));
//		obj.transform.SetParent ( goCreaturesParent.transform,false);
//		obj.name=obj.name.Replace ("(Clone)", oCreature.Id.ToString());
//	}
//}
