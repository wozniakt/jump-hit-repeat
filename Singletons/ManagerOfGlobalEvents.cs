﻿using UnityEngine;
using System.Collections;

public class ManagerOfGlobalEvents : MonoBehaviour
{

	public static ManagerOfGlobalEvents instance;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}

	public delegate void ShowText(string textToShow, Vector2 pos);
	public event ShowText eShowText;

	public void TriggerShowText(string textToShow, Vector2 pos){
		if (eShowText!=null) {
			eShowText (textToShow,  pos); 
		}
	}


	public delegate void UpdateStoryText(StoryState storyState);
	public event UpdateStoryText eUpdateStoryText;

	public void TriggerUpdateStoryText(StoryState storyState){
		if (eUpdateStoryText!=null) {
			eUpdateStoryText (storyState); 
		}
	}

	public delegate void UpdateData();
	public event UpdateData eUpdateData;

	public void TriggerUpdateData(){
		if (eUpdateData!=null) {
			eUpdateData (); 
		}
	}

	public delegate void ChangeGameState(GameState gameState);
	public event ChangeGameState OnChangeGameState;

	public void TriggerOnChangeGameState(GameState gameState){
		if (OnChangeGameState!=null) {
			OnChangeGameState (gameState); 
		}
	}

	public delegate void NotEnoughMoney(string TextToShow);
	public event NotEnoughMoney eNotEnoughMoney;

	public void TriggerNotEnoughMoney(string TextToShow){
		if (eNotEnoughMoney!=null) {
			eNotEnoughMoney (TextToShow); 
		}
	}


	public delegate void ChangeMonster(Creature oMonster);
	public event ChangeMonster eChangeMonster;

	public void TriggerChangeMonster(Creature oMonster){
		if (eChangeMonster!=null) {
			eChangeMonster (oMonster); 
		}
	}



	public delegate void GetPoints(int points);
	public event GetPoints OnGetPoints;

	public void TriggerOnGetPoints(int points){
		if (OnGetPoints!=null) {
			OnGetPoints (points);// 
		}
	}


	public delegate void LifeLose(int hp);
	public event LifeLose OnLifeLose;

	public void TriggerOnLifeLoses(int hp){
		if (OnLifeLose!=null) {
			OnLifeLose (hp);// 
		}
	}

	public delegate void BossOnScene(bool switchOn);
	public event BossOnScene eBossOnScene;

	public void TriggerBossOnScene(bool switchOn){
		if (eBossOnScene!=null) {
			eBossOnScene ( switchOn);// 
		}
	}


}

