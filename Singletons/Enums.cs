﻿using UnityEngine;
using System.Collections;

public enum CreatureState {Idle, Walk, Run, JumpForward, MeleeAttack, MagicAttack, FloatingMagicAttack, Hurt,

	JumpUp, JumpDown, HasBeenHit,PowerUpNow, IsPreparingToJump, Dead }
public enum HeroType {Warrior, Wizard}

public enum StoryState {ShowLetters, ShowChests}

public enum GameState {Menu, GameOn, GamePause, GameLost, GameResume}

public enum BulletType {EarthQuake,Arrow,Rock, Fireball_Hero, IceBall, Sword_Hero, Flame_Monster}

public enum EffectType {SmokeWhite,ExplosionWhite,HitWhite,EarthExplosion, Coin,BombExplosion,ExplosionDeath, CoinUIText,ExplosionHitMonster}

public enum PowerUpType {PowerUp1, PowerUp2, PowerUp3}

//public enum MonsterState {Idle,JumpUp, FallingDown, HitGround, HasBeenHit,PowerUpNow}

public enum AbilityType {SuperPower, HyperPower,MegaPower }

public enum SoundType
{
	HitGoblin, Earthquake, Fireball, PowerUp, JumpUp, CoinCollect,Charge, MonsterHit, AbilityActivate
}


