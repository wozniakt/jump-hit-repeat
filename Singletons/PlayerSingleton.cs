﻿using UnityEngine;
using System.Collections;

public class PlayerSingleton : MonoBehaviour
{
	public static PlayerSingleton instance;

	private void Awake()
	{
		//DontDestroyOnLoad(transform.root.gameObject);
		if (instance == null)
			instance = this;
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}
	}

}
