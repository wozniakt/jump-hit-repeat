﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour {

	public static string PREFAB_PATH = "Prefabs/";
	public  PlayerData oPlayerData;
	public static DataManager instance;
	public GameState oGameState;

	void Awake ()
	{

		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
		loadData ();
//		Debug.Log (DataManager.instance.oPlayerData.CurrentMonster.Id +"   "+ DataManager.instance.oPlayerData.MonstersList [0].Id + "!!!");
//		if (DataManager.instance.oPlayerData.CurrentMonster==null) {
//			ManagerOfGlobalEvents.instance.TriggerChangeMonster (DataManager.instance.oPlayerData.MonstersList [0]);
//		}
	
	}
	// Use this for initialization
	void Start () {
		ManagerOfGlobalEvents.instance.OnChangeGameState += this.ChangeGameState;
		ManagerOfGlobalEvents.instance.OnGetPoints += this.UpdateCoins;
		ManagerOfGlobalEvents.instance.OnLifeLose += this.UpdateHp;
		ManagerOfGlobalEvents.instance.eChangeMonster += this.ChangeCurrentMonster;

		oPlayerData.CoinsCount = oPlayerData.CoinsCount; 
		oPlayerData.HighscorePointsCount = PlayerPrefs.GetInt ("HighScore");
		oPlayerData.PointsCount = 0;
		//oPlayerData.HpCount = 3;
		oPlayerData.ComboCount=1;
		oGameState = new GameState ();
		if (DataManager.instance.oPlayerData.CurrentMonster.Id==0) {
			ManagerOfGlobalEvents.instance.TriggerChangeMonster (DataManager.instance.oPlayerData.MonstersList [0]);
		}
	}

	public void ChangeCurrentMonster(Creature oMonster){
		
		oPlayerData.CurrentMonster=oMonster;
	}


	public void  UpdateCoins(int points){
		if (oGameState==GameState.GameOn) {

				oPlayerData.CoinsCount = oPlayerData.CoinsCount+(points*1);
		
			}		
			//oPlayerData.HighscorePointsCount = Mathf.Max (oPlayerData.HighscorePointsCount, oPlayerData.PointsCount);
			ManagerOfGlobalEvents.instance.TriggerUpdateData ();
	}


	public void  UpdateHp(int hp){

	//to jest powielenie tego co ctrlTakingDamageMonster-prawdpopodnioe	oPlayerData.CurrentMonster.Hp = oPlayerData.CurrentMonster.Hp-hp;
//		///////Debug.Log ("before should game lost :"+oPlayerData.CurrentMonster.Hp);
//		if (oPlayerData.CurrentMonster.Hp <=0) {
//			///////Debug.Log ("Should game lost");
//			ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameLost);
//		}
//		ManagerOfGlobalEvents.instance.TriggerUpdateData ();
	}

	public void  ChangeGameState (GameState gameState)
	{

		oGameState = gameState;
		//DataManager.instance.oGameState = gameState;
		switch (oGameState) {
		case GameState.GameOn:
			Debug.Log ("current ,oster: "+oPlayerData.CurrentMonster.Id);
			if (oPlayerData.CurrentMonster==null) {
				ManagerOfGlobalEvents.instance.TriggerChangeMonster (DataManager.instance.oPlayerData.MonstersList [0]);
			}
			if (oPlayerData.CurrentMonster.Id==null) {
				ManagerOfGlobalEvents.instance.TriggerChangeMonster (DataManager.instance.oPlayerData.MonstersList [0]);
			}
			oPlayerData.CurrentMonster.Hp = oPlayerData.CurrentMonster.MaxHp;
			GameManager.instance.createScene ("Cave");
			Time.timeScale = 1;
			break;
		case GameState.GameResume:
			Time.timeScale = 1;
			oGameState =  GameState.GameOn;
			break;
		case GameState.Menu:
			Time.timeScale = 1;
			break;
		case GameState.GamePause:
			Time.timeScale = 0;
			break;

		case GameState.GameLost:
			PlayerPrefs.SetInt ("HighScore",oPlayerData.HighscorePointsCount);
			//yield return new WaitForSeconds (1);
	
			Time.timeScale = 0.01f;
			break;

		}
	}

	public void ReStartGame(){
		StartCoroutine (ReStartGameCoroutine ());
		UI_Manager.instance.StopAllCoroutines();
	}

	public IEnumerator ReStartGameCoroutine(){

		AsyncOperation async = SceneManager.LoadSceneAsync(0); 
		async.allowSceneActivation = true;
		yield return async;
		ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameOn);
		oPlayerData.ComboCount = 0;
		oPlayerData.PointsCount = 0;
		StoryManager.instance.reInitStory ();
		//oPlayerData.HpCount = 3;
		//UI_Manager.instance.createScene ();
		//UI_Manager.instance.UpdateHudTexts ();
		//UI_Manager.instance.UpdateHpText ();

		Time.timeScale = 1;
		ManagerOfGlobalEvents.instance.TriggerUpdateData ();
		//StoryManager.instance.reInitStory ();
	}

	public void PauseGame(){


		ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GamePause);
		saveData ();
	}

	public void ResumeGame(){
		ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.GameResume);
	}

	public void ExitGame(){
		PlayerPrefs.SetInt ("CoinsCount",oPlayerData.CoinsCount);
		PlayerPrefs.SetInt ("HighScore",oPlayerData.HighscorePointsCount);
		Application.Quit ();
	}

	public void BackToMenu(){
		saveData ();
		if (DataManager.instance.oGameState!=GameState.Menu) {
			ManagerOfGlobalEvents.instance.TriggerOnChangeGameState (GameState.Menu);
		}
		UI_Manager.instance.ButtonQuick_Restart.StopAllCoroutines ();
		UI_Manager.instance.StopCoroutine("finishGame" );
	}


	public void saveData ()
	{
		SaveLoadData.Save (oPlayerData);
	}

	public void loadData ()
	{
		SaveLoadData.Load ();
		if (SaveLoadData.LoadedPlayerData != null) {
			oPlayerData = SaveLoadData.LoadedPlayerData;
		}

		//UI_Manager.instance.UpdateHudTexts ();

	}


	public void ClearPLayerData ()
	{
		SaveLoadData.ClearData ();

	}
}