﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerData 
{
	[SerializeField]
	Creature currentMonster;
	public Creature CurrentMonster
	{
		get
		{return currentMonster;}
		set{

			currentMonster= value;
			//coinsCount = DataManager.instance.coins;
		}
	}
	[SerializeField]
	List<Creature> monstersList;
	public List<Creature> MonstersList{
		get
		{return monstersList;}
		set{

			monstersList = value;  //new List<Creature> (DataManager.instance.heroesList);
			//		coinsCount = DataManager.instance.coins;
		}
	}


	[SerializeField]
	float comboCount;
	public float ComboCount
	{
		get
		{return comboCount;}
		set{

			comboCount= value;

		}
	}



	[SerializeField]
	int pointsCount;
	public int PointsCount
	{
		get
		{return pointsCount;}
		set{

			pointsCount= value;

		}
	}



	[SerializeField]
	int highscorePointsCount;
	public int HighscorePointsCount
	{
		get
		{return highscorePointsCount;}
		set{
			if (highscorePointsCount<value) {
				highscorePointsCount= value;
			}

		}
	}


	[SerializeField]
	int coinsCount;
	public int CoinsCount
	{
		get
		{return coinsCount;}
		set{
			if (value>=0) {
				coinsCount= value;
			} else {
				coinsCount = 0;		
			}	

			PlayerPrefs.SetInt ("CoinsCount", CoinsCount);
		}
	}
		
}